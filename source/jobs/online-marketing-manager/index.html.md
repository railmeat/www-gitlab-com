---
layout: markdown_page
title: "Online Marketing Manager"
---

## Responsibilities

* Experience with Marketo to help the team manage newsletters, leads, and analytics. Not afraid to dig around in data to help create reports for the team meetings. Comfortable being a self-taught marketing automation master.  
* You will lead our online ads strategy and must have experience with Google Adwords, etc.
* Work with our demand generation manager to help implement email nurture campaigns at all stages of the funnel.   
* Work with demand generation to make sure the leads flowing from our marketing automation software to Salesforce are correct and timely.
* Work with sales and our demand generation lead to make sure the proper reports are generated in order to track leads.
