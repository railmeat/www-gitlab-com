---
layout: markdown_page
title: "Content Marketing"
---

Welcome to the Content Marketing Handbook

[Up one level to the Product Marketing Handbook](../) {::comment} TIP FOR ONE LEVEL UP :)  {:/comment}

## On this page
{:.no_toc}

* Will be replaced with the ToC, excluding the "On this page" header
{:toc}

## Introduction<a name="intro"></a>

The philosophy for Content Marketing at GitLab is to help bring insider
knowledge and implicit practice to the hands of developers getting acquainted
with GitLab, and decision makers considering GitLab.

We want to meet each user where they are now, and help them be the most
efficient they can be with our tools.
We want to help their teams realize their creative and collaborative goals.

We also want to address the whole organization, the coal-face of development
or in decision making roles. GitLab is not only used for code development and
review. Those same tools are used by team members to track progress, create
documentation and collaborate on projects. When we show how we work "inside
GitLab" we also model how to use our software.

## 2016 activities<a name="2016"></a>

- Publish an active [blog](blog/) with useful content relevant to GitLab users.
- Host [webcasts](webcasts/)
which welcome newcomers, celebrate with existing users, and provide access to expertise.
- Publish a twice-monthly email newsletter you can sign up to on [our contact page](https://about.gitlab.com/contact/).

## What is the webcast program at GitLab?<a name="webcast"></a>

-   The webcast program consists of regular online events for GitLab users and community members.
-   The webcasts are recorded and distributed to GitLab users, and can be referred to in the Resource Library as part of GitLab University

- [How to Schedule a webcast](#schedule)
- [Technical requirements to view a webcast](#view)


### Monthly Program

This program is in development. For the first month, January, we'll start with one webcast, then add another and build up to the full schedule.

### Welcome to GitLab - 20 mins (coming soon!)

-   Live demo run.
-   Similar base demo script each month.
    -   How to use GitLab - essential demo.
    -   This month’s Q+A (commonly asked questions, recent questions from twitter)
    -   GitLab’s products and services.
    -   How to find stuff in the GitLab; getting help, direction, participation.
    -   Community welcome mat: How to meet other GitLab users what events we’ll be at, how about you?
-   Aimed at developers or decision makers who have signed up in the last 30 days, inviting them into the community.

### Release Party - 30 - 40 mins

-   Monthly Thursday following a release.
-   Present highlights from the new features.
-   Refer to any resources, docs, screencasts, etc.
-   Guest speakers from the dev team about the new features.
-   Highlight contributors and the MVP for that month.
-   New contributors welcomed.
-   Q+A from audience.

### GitLab Tutorials - 40 mins - 1 hour

-   Live presentation, demo or discussion on monthly in-depth learning theme.
-   Preceded by 3 weeks of blog posts, screencasts, tutorial and an invitation which leads to the online event.
-   Live event includes: Guest speaker / interview / presentation / demo as appropriate to topic.
-   Q+A from audience. Survey to GitLab users on the topic if appropriate.
-   After event: Blog post of findings from the Q+A, results of survey.
-   Roll-up content into an downloadable ebook, course or other way to make the content more easily accessed and reviewed.

## Scheduling webcasts<a name="schedule"></a>

- Webcasts are on Thursdays, 17:00 UTC (9am PST, 6pm CET)
- Panelists should arrive 15 mins before the webcast for a final sound check
- Panelists should participate in a rehearsal before the webcast

#### Webcast configuration checklist

- Set up webinar in Gotowebinar by cloning the most recent webcast.
- Add a welcome message to attendees
- Upload any handouts (up to 5)
- Upload images, branding and speaker photos (must be jpg or gif and the exact dimensions)

#### Create a calendar event

GoToWebinar doesn't send out a Google calendar friendly invite

- Create an invitation for speakers in Google calendar
- Include their actual URL from their invite.
- Do the same for the rehearsal invitation.


#### Create the program in Marketo

- Clone the last campaign
- Connect the webcast campaign to Gotowebinar. Webcast > Event Settings > Event partner
- Correct version number and invitation date and details for assets:
    -   The landing page
    -   The confirmation email
- Update the hero form on the landing page to the correct form.
- Final check: If the lightbulb is not "on" (yellow), then it's not doing anything.
Check the smart list and flow first. To activate: Click the "registered" smart campaign -> "Schedule" tab -> "Activate" button

#### Promote

- Publish to Facebook
- Schedule tweets
- Create a blog post
- Add to next newsletter

#### As the event starts

- Promote [the appropriate attendees to panelist](https://support.citrixonline.com/en_US/webinar/knowledge_articles/000027765)
- Conduct a sound check and sharing check for anyone who will present.
- Organizers and Panelists are listed in the "Staff" tab and they can mute and unmute attendees, and see questions sent to the Panelists, etc.

## After the webcast<a name="followup"></a>

- Process the recording as .mov
- Upload to YouTube and slides to Speakerdeck
- Blog post to share the recording and slides


### Viewing webcasts<a name="view"></a>

- [Citrix Online system requirements](https://support.citrixonline.com/webinar/all_files/G2W010003)
- Using GoToWebinar Instant Join, Linux/Unbuntu users can view in a web browser.
- Linux users should use Chromium to view the browser.

----

## GitLab Blog 
{: #blog}

### Objectives &amp; Purposes

- Encourage potential users to try GitLab
- Motivate our community to explore what's best from GitLab features
- Provide accurate, interesting and new information

### Important considerations

The [blog](/blog) is our main publishing outlet. Let's do our best to show what's best!
 
- Content should communicate the benefits of GitLab's unique innovations and tools (e.g., CI)
- We want to bring in voices from all throughout the company, as well as 
from GitLab users and our customers.
- As always, **everyone can contribute** - GitLab Team members and [Community Writers]

### Posts Formats

We aim to publish content multiple times a week, with a reliable publishing schedule. The user will most likely have a 
pleasant experience if we combine multiple posts formats, trying never to be too much repetitive. Repetition is boring. 
<!-- According to our [Content Marketing Strategy][WE'D NEED A LINK], --> We'll alternate between the following formats:

- Short form articles
- Long form articles
- Release announcements
- Feature highlights
- Tutorials
- Inside GitLab

## Blog Content

It is important to have in mind that a good post can considerably reach a lot of people and acquire more users.
With well written articles, we have the opportunity to expose and explore, in a friendly language, all the features GitLab provides.

Try to think about what our users might have interest in reading before picking up a subject to work on.
And remember, our users do not need to be super advanced programmers. They can be newbie folks with limited experience, students,
technology enthusiasts, and people not used to Git, version control and continuous integration. That's why we need to [define a target audience][tech-writing-audience] before start writing.

Our audience will probably be interested in the topics listed below. 

### Product-specific topics

- Tutorials on using GitLab, GitLab CI, GitLab Runners, GitLab Geo, GitLab Pages, etc.
- Feature highlights bring attention to specific features at GitLab
- GitLab Workflow
- Key features overviews
- New feature releases

### User Stories

- Contributor stories "why I contribute to GitLab"
- Use case stories "how we use GitLab"
- Boss stories "how GitLab enabled innersourcing"
- Inception stories "how GitLab uses GitLab"
- Adoption stories "how we switched from SVN to GitLab"
- Customer stories "why we choose GitLab"

Do you have a better idea? Don't hesitate, [create an issue][blog-tracker] with your proposal and we'll be glad to look into it.

### Media

- Videos with good screencasts, great subtitles, and narratives are expensive but popular, and hard to copy (what does happens to written content). For reference, [Realm.io] does a lot of good videos, for example [about Swift]
<!-- - Check out our "Screencast Guidelines", if that's the case => CREATE SCREENCAST GUIDELINES -->
- Try to work with images. At least choose one for the page cover
- Do not use an image if you are not certain that it is [public domain]
- Always provide a link to the original source of the image
- Compress your image. Use [TinyPNG] or a similar tool

## Technical Aspects

### Publishing process for GitLab Team members

1. Choose a [topic](#blog-content)
1. Define the target audience, knowledge level and system requirements ([example])
1. Create outlines (a few items describing what you want to discuss along the post)
1. Submit an issue on the [blog posts issue tracker][blog-tracker] containing the previous items
1. Mention @amara for feedback on your proposal and on your outlines 
1. Amara will evaluate the priority and estimate a due date for publishing
1. Write the post according to the [Professional Writing Techniques][writing-tech]
1. Submit your draft as a WIP MR (work in progress merge request) in the [GitLab website project][gitlabwww]
1. You'll get reviewed and feedback from our Marketing Team
1. Your post will be published

Not a GitLab Team member? Check the process for [Community Writers] below.

### Publishing process for Community Writers

For our [community writers], we will follow the Scalable Writing System described below. 

1. Guest Writer - choose a subject:
   - Make sure you are familiar with [GitLab Workflow]
   - Select an issue from <https://gitlab.com/gitlab-com/blog-posts/issues> or create a new one.
   - Leave a comment "@amara I would like to write this and I accept the terms on [Community Writers Program][Community Writers]. Below follows my writing sample."
2. Content Marketing - analyse the proposal:
  - Amara will evaluate the writer's sample and discuss anything necessary before start writing
  - When the guest writer is approved to get started, Amara will leave a comment "@username, you got it!" and assign the issue to the writer
3. Guest Writer: prepare local environment and submit the article
  - Fork <https://gitlab.com/gitlab-com/www-gitlab-com/> and run it locally
  - Write according to the [Professional Writing Techniques][writing-tech]
  - Submit a [WIP MR] with the proposal and assign it to Amara
4. Reviewers: 
  - Amara will take a first look to approve the article for review, and assign Marcia for the first review
  - When first review is finished, Marcia will assign Axil for a detailed technical review
  - When finished, Axil will reassing the MR to Amara, who will follow the [check list](#check-list-before-merging) and approve the content for publishing
5. Content Marketing: publish
  - Content Marketing will place the date for publishing
  - Amara will merge and tweet
6. Content Marketing / Account Ops: pay the writer
  - Amara email the writer to wire the money
  - Guest writer will get paid

### Blog Post Issue Tracker

To keep things clear for everyone, we assume:

- Anything not assigned to a person is in the [backlog]
- Anything that is assigned to a person is "in progress"
- Anything that has a WIP MR is ready for review

### Styles

- Check out our [styles guidelines]
- Amuse yourself with the power of [Kramdown], our markdown engine for about.GitLab.com <!-- => LINK TO MARKDOWN DOC WHEN READY-->

### Forked project

Before you write, make sure you forked [`www-gitlab-com`], cloned to your computer, and were able to preview it locally by running `bundle exec middleman`. 
Before making any change, create a new branch `git checkout -b branchname` cloned from `master`.

### Check list before merging

Reviewer - check these before you publish:

- First instance of GitLab should be linked to [GitLab] <!-- => WHAT EXACTLY DOES IT MEAN? -->
- Follow the [Blog post style guide]
- Check all links - make sure none is broken
- Check the file extension `.html.md`
- Check the date on the file name
- Check the date in the post
- Check the image(s) is(are) crunched down.
- Check the blog appears good locally
- When you have double checked, you can merge

It takes about 5 mins for the blog post to appear as published.

After the blog post is published we should tweet immediately from the GitLab
Twitter account, and schedule follow up tweets and LinkedIn and Facebook.

## Get inspired

- The content doesn't have to be about GitLab, it can also be other content aimed at developers, Hacker News or team leads
- You need to have high quality and high volume, great times are in the [Priceonomics content marketing handbook]
- When submitting to Hacker News please add a ? to the url and do not announce it anywhere to prevent setting off the voting ring detector
- What worked for Apigee was the 'collaboration in the 21st century' theme
- Explore a reading club such as [a NoSQL summer]
- Milk [GitLab Flow] for more blog posts and videos

## Inspire

We invite and encourage guest writers and also offer compensation through the [Community Writers] program.

[a NoSQL summer]: //nosqlsummer.org/
[about Swift]: https://realm.io/news/top-5-swift-videos-of-2014/
[backlog]: https://dev.gitlab.org/gitlab/blog-posts/issues?milestone_id=&scope=all&sort=created_desc&state=opened&utf8=%E2%9C%93&author_id=&assignee_id=0&milestone_title=&label_name=
[Blog post style guide]: https://gitlab.com/gitlab-com/blog-posts/blob/master/STYLEGUIDE.md
[blog-tracker]: https://gitlab.com/gitlab-com/blog-posts/issues
[Community Writers]: https://about.gitlab.com/community/writers
[example]: ../../developer-relations/technical-writing/#st-subject-audience-requirements
[GitLab]: //about.gitlab.com
[GitLab Flow]: //doc.gitlab.com/ee/workflow/gitlab_flow.html
[GitLab Workflow]: https://www.youtube.com/watch?v=enMumwvLAug "Introduction to GitLab Workflow"
[gitlabwww]: https://gitlab.com/gitlab-com/www-gitlab-com
[Kramdown]: //kramdown.gettalong.org/
[Priceonomics content marketing handbook]: http://priceonomics.com/the-content-marketing-handbook/
[public domain]: https://en.wikipedia.org/wiki/Public_domain
[Realm.io]: //realm.io
[styles guidelines]: ../../developer-relations/technical-writing/#styles-guidelines 
[tech-writing-audience]: ../../developer-relations/technical-writing/#st-subject-audience-requirements
[tinypng]: //tinypng.com
[WIP MR]: http://docs.gitlab.com/ce/workflow/wip_merge_requests.html "Work In Progress Merge Request"
[writing-tech]: ../../developer-relations/technical-writing/#professional-writing-techniques
[`www-gitlab-com`]: https://gitlab.com/gitlab-com/www-gitlab-com/
